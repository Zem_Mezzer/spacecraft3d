﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] AudioSource audioSource = null;
    [SerializeField] AudioClip DeathSound = null;

    public static AudioManager AMInstance;
    void Start()
    {
        if (AMInstance == null)
            AMInstance = this;
        else
            Destroy(gameObject);
    }

    public void PlayDeathSound()
    {
        audioSource.PlayOneShot(DeathSound);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private PlayerController Player;

    void Update()
    {
        if (_GameManager.GMInstance.playerisalive&&Input.GetMouseButton(0))
        {
            if(Player == null)
            {
                Player = _GameManager.GMInstance.player.GetComponent<PlayerController>();
            }
            ICommand command = new FlyCommand();
            CommandInvoker.AddCommand(command);
        }
        else
        if(!_GameManager.GMInstance.playerisalive&&Input.GetMouseButtonDown(0))
        {
            ICommand command = new RespawnCommand();
            CommandInvoker.AddCommand(command);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Concurrent;


public class MeteorManager : MonoBehaviour
{
    public static MeteorManager MMIntance;

    public float offset = 1;

    [SerializeField] GameObject Meteor = null;
    public List<MeteorBehaviour> Meteors;

    [SerializeField] int MeteorCount = 0;

    ObjectPool<MeteorBehaviour> MeteorPool;
    


    private void Start()
    {
        Meteors = new List<MeteorBehaviour>();
        MeteorPool = new ObjectPool<MeteorBehaviour>(Meteor.GetComponent<MeteorBehaviour>());

        if(MMIntance == null)
        {
            MMIntance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void MeteorActivate()
    {
        for(int i = 0; i < MeteorCount; i++)
        {
            Meteors.Add(MeteorPool.Get());
            Meteors[i].retransform();
        }
    } 

    public void MeteorDeactivate()
    {
        offset = 1;
        for (int i = 0; i < MeteorCount; i++)
        {
            MeteorPool.Put(Meteors[i]);
        }
        Meteors.Clear();
    }

    private void Update()
    {
        if (_GameManager.GMInstance.playerisalive)
        {
            offset += Time.deltaTime / 10;
        }
    }

}

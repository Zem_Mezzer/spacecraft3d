﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScoreManager : MonoBehaviour
{
    public static ScoreManager SMIntance;
    [HideInInspector] public float playertime = 0;
    [HideInInspector] public int _totalscore;


    void Awake()
    {
        if (SMIntance == null)
        {
            SMIntance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (_GameManager.GMInstance.playerisalive)
        {
            IncScore();
        }
    }

    public int GetScore()
    {
        return PlayerPrefs.GetInt("HighScore", 0);
    }

    public void CheckScore()
    {
        if(_totalscore > PlayerPrefs.GetInt("HighScore", 0))
        {
            SetScore();
        }
        UiManager.UMInstance.SetHighScore();
    }

    void SetScore()
    {
        PlayerPrefs.SetInt("HighScore", _totalscore);
    }

    void IncScore()
    {
        playertime += Time.deltaTime;
        _totalscore = (int)playertime;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager:MonoBehaviour
{
    public static AnalyticsManager AMInstance;

    private void Start()
    {
        if(AMInstance == null)
        {
            AMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void ResultSend()
    {
        int HighScore = PlayerPrefs.GetInt("HighScore");
        int TotalScore = ScoreManager.SMIntance._totalscore;
        Dictionary<string, object> Score = new Dictionary<string, object>();
        Score.Add("HighScore", HighScore.ToString());
        Score.Add("TotalScore", TotalScore.ToString());
        string UserScore = string.Format("Player High score: {0}, Player Total Score {1}", HighScore, TotalScore);
        Debug.Log(string.Format("Event send: ({0})", UserScore));
        Analytics.CustomEvent("Game_Over", Score);
    }
}

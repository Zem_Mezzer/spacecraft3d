﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationManager : MonoBehaviour
{
    public static AnimationManager AMInstance;

    [SerializeField] RectTransform Tooltip, TotalScore, HighScore = null;
    [SerializeField] float AnimationTime;

    void Start()
    {
        if(AMInstance == null)
        {
            AMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        UIHide();
    }

    //private void Update()
    //{
    //    //UI.SetBool("ui_show", !GameLevelManager.GLMInstance.playerisalive);
    //}

    public void UIShow()
    {
        Tooltip.DOAnchorPosY(1000,AnimationTime);
        TotalScore.DOAnchorPosY(600, AnimationTime);
        HighScore.DOAnchorPosY(1000, AnimationTime);
    }

    public void UIHide()
    {
        Tooltip.DOAnchorPosY(0, AnimationTime);
        TotalScore.DOAnchorPosY(1000, AnimationTime);
        HighScore.DOAnchorPosY(600, AnimationTime);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine;


public class AdManager : MonoBehaviour
{
    int AdCount = 0;
    public static AdManager AMInstance;
    

    void Start()
    {
        if(AMInstance == null)
        {
            AMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        if (Advertisement.isSupported)
        {
#if UNITY_IOS
             Advertisement.Initialize("3475422");
#elif UNITY_ANDROID
            Advertisement.Initialize("3475423");
#elif UNITY_EDITOR
            Debug.Log("AD");
#endif
        }
    }
    public void ADShow()
    {
        AdCount++;
        if (Advertisement.IsReady()&&AdCount%5==0)
        {
            Advertisement.Show();
        }
    }

}

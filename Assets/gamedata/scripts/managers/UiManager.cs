﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static UiManager UMInstance;
    public Text HighScore;
    public Text TotalScore;

    void Start()
    {
        if(UMInstance == null)
        {
            UMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        SetHighScore();
    }

    private void Update()
    {
        if (_GameManager.GMInstance.playerisalive)
        {
            TotalScore.text = ScoreManager.SMIntance._totalscore.ToString();
        }
    }

    public void SetHighScore()
    {
        HighScore.text = ScoreManager.SMIntance.GetScore().ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class _GameManager : MonoBehaviour
{
    [HideInInspector] public GameObject player = null;
    public bool playerisalive = false;
    

    GameObject _player;


    public static _GameManager GMInstance;

    private void Start()
    {
        if(GMInstance == null)
        {
            GMInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void GameStart()
    {
        //Death Action Instance 
        if(_player == null)
        {
            _player = Instantiate(player);
            _player.GetComponent<PlayerController>().onPlayerDeath = () =>
            {
                AudioManager.AMInstance.PlayDeathSound();
                playerisalive = false;
                ScoreManager.SMIntance.CheckScore();
                AnalyticsManager.AMInstance.ResultSend();
                AdManager.AMInstance.ADShow();
                AnimationManager.AMInstance.UIHide();
            };
        }

        _player.SetActive(true);
        _player.transform.position = new Vector3(0, 0, 0);

        MeteorManager.MMIntance.MeteorActivate();
    }

    public void Restart()
    {
        GameStart();
        ScoreManager.SMIntance.playertime = 0;
        playerisalive = true;
        AnimationManager.AMInstance.UIShow();
    }
}
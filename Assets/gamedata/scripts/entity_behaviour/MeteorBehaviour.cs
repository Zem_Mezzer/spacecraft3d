﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorBehaviour : MonoBehaviour
{
    [SerializeField] Rigidbody rb = null;
    [SerializeField] GameObject MeteorDestroyParticle = null;

    private void Start()
    {
        MeteorDestroyParticle.transform.parent = null;
    }

    void Update()
    {
        
        rb.velocity = Vector2.left * MeteorManager.MMIntance.offset;

        if (transform.position.x <= -4f)
        {
            retransform();
        }

        if (!_GameManager.GMInstance.playerisalive)
        {
            MeteorManager.MMIntance.MeteorDeactivate();
        }

    }

    public void retransform()
    {
        transform.position = new Vector3(Random.Range(5f,10f), Random.Range(-5.5f,5.5f), 0);
        transform.Rotate(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
    }

    public void destroy_particles_instance()
    {
        try
        {
            MeteorDestroyParticle.transform.position = transform.position;
            MeteorDestroyParticle.transform.rotation = transform.rotation;
            MeteorDestroyParticle.GetComponent<ParticleSystem>().Play();
        }
        catch { }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            retransform();
        }
    }

    private void OnDisable()
    {
        destroy_particles_instance();
    }
}

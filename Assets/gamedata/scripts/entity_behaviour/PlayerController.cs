﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float _flyspeed = 0;
    [SerializeField] GameObject Sparks = null;
    [SerializeField] Rigidbody rb;

    public Action onPlayerDeath;

    public static PlayerController PCInstance;

    private void Start()
    {
        if(PCInstance == null)
        {
            PCInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        Sparks.transform.parent = null;
    }
    void Update()
    {
        transform.position = new Vector2(transform.position.x, Mathf.Clamp(transform.position.y, -5.3f, 4.8f));
    }

    public void Fly()
    {
        rb.velocity = Vector2.zero;
        rb.velocity = new Vector2(rb.velocity.x,_flyspeed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        onPlayerDeath?.Invoke();
        destroy_particles_instance();
        gameObject.SetActive(false);
    }

    private void destroy_particles_instance()
    {
        Sparks.transform.position = transform.position;
        Sparks.transform.rotation = transform.rotation;
        Sparks.GetComponent<ParticleSystem>().Play();
    }
}

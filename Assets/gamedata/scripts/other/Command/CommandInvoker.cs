﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandInvoker : MonoBehaviour
{
    static Queue<ICommand> commands;

    private void Awake()
    {
        commands = new Queue<ICommand>();
    }

    public static void AddCommand(ICommand command)
    {
        commands.Enqueue(command);
    }

    void Update()
    {
        if (commands.Count > 0)
        {
            commands.Dequeue().Execute();
        }
    }
}

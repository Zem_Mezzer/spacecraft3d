﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyCommand : ICommand
{
    public void Execute()
    {
        PlayerController.PCInstance.Fly();
    }
}

public class RespawnCommand : ICommand
{
    public void Execute()
    {
        _GameManager.GMInstance.Restart();
    }
}

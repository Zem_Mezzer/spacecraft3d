﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T:MonoBehaviour
{
    private Stack<T> pool;
    private readonly T Obj;

    public ObjectPool(T _Obj)
    {
        pool = new Stack<T>();
        Obj = _Obj;
    }

    public T Get()
    {
        var obj = pool.Count > 0 ? pool.Pop() : Object.Instantiate(Obj);
        obj.gameObject.SetActive(true);
        return obj;
    }

    public void Put(T _Obj)
    {
        _Obj.gameObject.SetActive(false);
        pool.Push(_Obj);
    }
}
